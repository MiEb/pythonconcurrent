from concurrent.futures import ThreadPoolExecutor, Future
from time import sleep
from random import seed, randint


def task_to_execute(param1, param2):
    # Simulate task execution and multiple parameter processing by sleeping for param1 * param2 seconds
    sleep( param1 * param2 )
    # Provide a meaningfull return-value as param1 and param2 are random and we want the thread sleep time
    return param1 * param2

def task_completion_callback(task):
    print("The task was sleeping for " + str(task.result()) + " seconds")

seed()
with ThreadPoolExecutor(max_workers=4) as executor:
    for task_num in range(1, 10):
        task = executor.submit(task_to_execute, randint(1, 5), randint(1, 5))
        task.add_done_callback(task_completion_callback)
        print("Added task with number " + str(task_num))
